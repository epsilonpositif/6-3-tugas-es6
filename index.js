//Jawaban Soal 1
const hitung = (p, l) => {
    let panjang = p,
        lebar = l;

    luas = panjang * lebar;
    keliling = 2 * (panjang + lebar);
    console.log("Luasnya adalah " + luas + ". Kelilingnya adalah " + keliling)

}

hitung(5, 4);



//Jawaban soal 2
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: function() {
            console.log(`${firstName} ${lastName}`);
        }
    }
}

newFunction("William", "Imoh").fullName();


//Jawaban soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const { firstName, lastName, address, hobby } = newObject;

console.log(firstName, lastName, address, hobby);


//Jawaban soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
    // const combined = west.concat(east)
const combined = [...west, ...east];
console.log(combined);



//Jawaban soal 5
const planet = "earth";
const view = "glass";

var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;
console.log(before);